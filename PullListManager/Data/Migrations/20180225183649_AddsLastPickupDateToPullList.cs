﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace PullListManager.Data.Migrations
{
    public partial class AddsLastPickupDateToPullList : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<decimal>(
                name: "DepositAmount",
                table: "PullLists",
                nullable: true,
                oldClrType: typeof(decimal));

            migrationBuilder.AddColumn<DateTime>(
                name: "LastPickupDate",
                table: "PullLists",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "LastPickupDate",
                table: "PullLists");

            migrationBuilder.AlterColumn<decimal>(
                name: "DepositAmount",
                table: "PullLists",
                nullable: false,
                oldClrType: typeof(decimal),
                oldNullable: true);
        }
    }
}
