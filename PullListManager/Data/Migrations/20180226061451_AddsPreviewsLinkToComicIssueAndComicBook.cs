﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace PullListManager.Data.Migrations
{
    public partial class AddsPreviewsLinkToComicIssueAndComicBook : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "PreviewsLink",
                table: "ComicIssues",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PreviewsLink",
                table: "ComicBooks",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "PreviewsLink",
                table: "ComicIssues");

            migrationBuilder.DropColumn(
                name: "PreviewsLink",
                table: "ComicBooks");
        }
    }
}
