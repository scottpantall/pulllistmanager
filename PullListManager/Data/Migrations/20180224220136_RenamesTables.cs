﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace PullListManager.Data.Migrations
{
    public partial class RenamesTables : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ComicBook_PullList_PullListId",
                table: "ComicBook");

            migrationBuilder.DropForeignKey(
                name: "FK_PullList_Stores_StoreId",
                table: "PullList");

            migrationBuilder.DropForeignKey(
                name: "FK_PullListRequest_PullList_PullListId",
                table: "PullListRequest");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PullListRequest",
                table: "PullListRequest");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PullList",
                table: "PullList");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ComicBook",
                table: "ComicBook");

            migrationBuilder.RenameTable(
                name: "PullListRequest",
                newName: "PullListRequests");

            migrationBuilder.RenameTable(
                name: "PullList",
                newName: "PullLists");

            migrationBuilder.RenameTable(
                name: "ComicBook",
                newName: "ComicBooks");

            migrationBuilder.RenameIndex(
                name: "IX_PullListRequest_PullListId",
                table: "PullListRequests",
                newName: "IX_PullListRequests_PullListId");

            migrationBuilder.RenameIndex(
                name: "IX_PullList_StoreId",
                table: "PullLists",
                newName: "IX_PullLists_StoreId");

            migrationBuilder.RenameIndex(
                name: "IX_ComicBook_PullListId",
                table: "ComicBooks",
                newName: "IX_ComicBooks_PullListId");

            migrationBuilder.AlterColumn<int>(
                name: "StoreId",
                table: "PullLists",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddPrimaryKey(
                name: "PK_PullListRequests",
                table: "PullListRequests",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PullLists",
                table: "PullLists",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ComicBooks",
                table: "ComicBooks",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ComicBooks_PullLists_PullListId",
                table: "ComicBooks",
                column: "PullListId",
                principalTable: "PullLists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PullListRequests_PullLists_PullListId",
                table: "PullListRequests",
                column: "PullListId",
                principalTable: "PullLists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PullLists_Stores_StoreId",
                table: "PullLists",
                column: "StoreId",
                principalTable: "Stores",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ComicBooks_PullLists_PullListId",
                table: "ComicBooks");

            migrationBuilder.DropForeignKey(
                name: "FK_PullListRequests_PullLists_PullListId",
                table: "PullListRequests");

            migrationBuilder.DropForeignKey(
                name: "FK_PullLists_Stores_StoreId",
                table: "PullLists");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PullLists",
                table: "PullLists");

            migrationBuilder.DropPrimaryKey(
                name: "PK_PullListRequests",
                table: "PullListRequests");

            migrationBuilder.DropPrimaryKey(
                name: "PK_ComicBooks",
                table: "ComicBooks");

            migrationBuilder.RenameTable(
                name: "PullLists",
                newName: "PullList");

            migrationBuilder.RenameTable(
                name: "PullListRequests",
                newName: "PullListRequest");

            migrationBuilder.RenameTable(
                name: "ComicBooks",
                newName: "ComicBook");

            migrationBuilder.RenameIndex(
                name: "IX_PullLists_StoreId",
                table: "PullList",
                newName: "IX_PullList_StoreId");

            migrationBuilder.RenameIndex(
                name: "IX_PullListRequests_PullListId",
                table: "PullListRequest",
                newName: "IX_PullListRequest_PullListId");

            migrationBuilder.RenameIndex(
                name: "IX_ComicBooks_PullListId",
                table: "ComicBook",
                newName: "IX_ComicBook_PullListId");

            migrationBuilder.AlterColumn<int>(
                name: "StoreId",
                table: "PullList",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddPrimaryKey(
                name: "PK_PullList",
                table: "PullList",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_PullListRequest",
                table: "PullListRequest",
                column: "Id");

            migrationBuilder.AddPrimaryKey(
                name: "PK_ComicBook",
                table: "ComicBook",
                column: "Id");

            migrationBuilder.AddForeignKey(
                name: "FK_ComicBook_PullList_PullListId",
                table: "ComicBook",
                column: "PullListId",
                principalTable: "PullList",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PullList_Stores_StoreId",
                table: "PullList",
                column: "StoreId",
                principalTable: "Stores",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PullListRequest_PullList_PullListId",
                table: "PullListRequest",
                column: "PullListId",
                principalTable: "PullList",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
