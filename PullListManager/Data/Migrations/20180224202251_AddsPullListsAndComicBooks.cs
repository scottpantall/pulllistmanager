﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace PullListManager.Data.Migrations
{
    public partial class AddsPullListsAndComicBooks : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "PullList",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    CustomerEmail = table.Column<string>(nullable: true),
                    CustomerName = table.Column<string>(nullable: true),
                    CustomerPhone = table.Column<string>(nullable: true),
                    DateCreated = table.Column<DateTime>(nullable: false),
                    DepositAmount = table.Column<decimal>(nullable: false),
                    IsActive = table.Column<bool>(nullable: false),
                    Notes = table.Column<string>(nullable: true),
                    StoreId = table.Column<int>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PullList", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PullList_Stores_StoreId",
                        column: x => x.StoreId,
                        principalTable: "Stores",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "ComicBook",
                columns: table => new
                {
                    Id = table.Column<Guid>(nullable: false),
                    BookStatus = table.Column<int>(nullable: false),
                    DeliveryDate = table.Column<DateTime>(nullable: true),
                    OrderNumber = table.Column<string>(nullable: true),
                    PullListId = table.Column<int>(nullable: true),
                    Title = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_ComicBook", x => x.Id);
                    table.ForeignKey(
                        name: "FK_ComicBook_PullList_PullListId",
                        column: x => x.PullListId,
                        principalTable: "PullList",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateTable(
                name: "PullListRequest",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("SqlServer:ValueGenerationStrategy", SqlServerValueGenerationStrategy.IdentityColumn),
                    PullListId = table.Column<int>(nullable: true),
                    RequestTitle = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_PullListRequest", x => x.Id);
                    table.ForeignKey(
                        name: "FK_PullListRequest_PullList_PullListId",
                        column: x => x.PullListId,
                        principalTable: "PullList",
                        principalColumn: "Id",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_ComicBook_PullListId",
                table: "ComicBook",
                column: "PullListId");

            migrationBuilder.CreateIndex(
                name: "IX_PullList_StoreId",
                table: "PullList",
                column: "StoreId");

            migrationBuilder.CreateIndex(
                name: "IX_PullListRequest_PullListId",
                table: "PullListRequest",
                column: "PullListId");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "ComicBook");

            migrationBuilder.DropTable(
                name: "PullListRequest");

            migrationBuilder.DropTable(
                name: "PullList");
        }
    }
}
