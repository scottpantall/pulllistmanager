﻿using Microsoft.EntityFrameworkCore.Migrations;
using System;
using System.Collections.Generic;

namespace PullListManager.Data.Migrations
{
    public partial class HeyWhatsThis : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ComicBooks_PullLists_PullListId",
                table: "ComicBooks");

            migrationBuilder.DropForeignKey(
                name: "FK_PullListRequests_PullLists_PullListId",
                table: "PullListRequests");

            migrationBuilder.AlterColumn<int>(
                name: "PullListId",
                table: "PullListRequests",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AlterColumn<int>(
                name: "PullListId",
                table: "ComicBooks",
                nullable: false,
                oldClrType: typeof(int),
                oldNullable: true);

            migrationBuilder.AddForeignKey(
                name: "FK_ComicBooks_PullLists_PullListId",
                table: "ComicBooks",
                column: "PullListId",
                principalTable: "PullLists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_PullListRequests_PullLists_PullListId",
                table: "PullListRequests",
                column: "PullListId",
                principalTable: "PullLists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Cascade);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_ComicBooks_PullLists_PullListId",
                table: "ComicBooks");

            migrationBuilder.DropForeignKey(
                name: "FK_PullListRequests_PullLists_PullListId",
                table: "PullListRequests");

            migrationBuilder.AlterColumn<int>(
                name: "PullListId",
                table: "PullListRequests",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AlterColumn<int>(
                name: "PullListId",
                table: "ComicBooks",
                nullable: true,
                oldClrType: typeof(int));

            migrationBuilder.AddForeignKey(
                name: "FK_ComicBooks_PullLists_PullListId",
                table: "ComicBooks",
                column: "PullListId",
                principalTable: "PullLists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);

            migrationBuilder.AddForeignKey(
                name: "FK_PullListRequests_PullLists_PullListId",
                table: "PullListRequests",
                column: "PullListId",
                principalTable: "PullLists",
                principalColumn: "Id",
                onDelete: ReferentialAction.Restrict);
        }
    }
}
