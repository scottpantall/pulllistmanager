﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PullListManager.Data;
using PullListManager.Models;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace PullListManager.Controllers
{
    public class PullListController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public PullListController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;

        }

        // GET: PullList/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var currentUser = await _userManager.GetUserAsync(User);

            var pullList = await _context.PullLists
                .SingleOrDefaultAsync(m => m.Id == id);

            if (pullList == null)
            {
                return NotFound();
            }

            pullList.Books = await _context.ComicBooks.Where(b => b.PullListId == pullList.Id).ToListAsync();

            if(currentUser.RoleName == "Owner" && currentUser.StoreId != pullList.StoreId)
            {
                // Show "Not Authorized" page
                return RedirectToAction("NotAllowed", "Home");
            }

            return View(pullList);
        }

        //POST: PullList/Details/5
        [HttpPost]
        public async Task<IActionResult> Details(int? id, string[] comicIds)
        {
            foreach(var comicId in comicIds)
            {
                _context.ComicBooks.Remove(await _context.ComicBooks.FindAsync(Guid.Parse(comicId)));
            }
            var pullList = await _context.PullLists.FindAsync(id);
            pullList.LastPickupDate = DateTime.Now;

            await _context.SaveChangesAsync();
            return RedirectToAction("Details", "PullList", id);
        }

        // GET: PullList/Create
        public IActionResult Create(int? storeId)
        {
            return View();
        }

        // POST: PullList/Create
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Create(int id, [Bind("CustomerName,CustomerPhone,CustomerEmail,DepositAmount")] PullList pullList)
        {
            var currentUser = await _userManager.GetUserAsync(User);
            if (currentUser.RoleName == "Owner" && currentUser.StoreId != pullList.StoreId)
            {
                // Show "Not Authorized" page
                return RedirectToAction("NotAllowed", "Home");
            }

            if (ModelState.IsValid)
            {
                if(_context.PullLists.Any(p => p.CustomerEmail == pullList.CustomerEmail && p.StoreId == id))
                {
                    return View(pullList);
                }

                pullList.StoreId = id;
                pullList.IsActive = true;
                pullList.DateCreated = DateTime.Now;

                _context.Add(pullList);
                await _context.SaveChangesAsync();
                return RedirectToAction("Details", "Store", new { id = pullList.StoreId });
            }
            return View(pullList);
        }

        // Get: PullList/Edit/5
        public async Task<IActionResult> Edit(int? id)
        {
            var currentUser = await _userManager.GetUserAsync(User);
            if (currentUser.RoleName == "Owner" && currentUser.StoreId != id)
            {
                // Show "Not Authorized" page
                return RedirectToAction("NotAllowed", "Home");
            }

            if (id == null)
            {
                return NotFound();
            }

            var pullList = await _context.PullLists.SingleOrDefaultAsync(p => p.Id == id);
            return View(pullList);
        }

        // POST: PullList/Edit/5
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int pullListid, [Bind("Id,CustomerName,CustomerPHone,CustomerEmail,DepositAmount")] PullList pullList)
        {
            if(pullListid != pullList.Id)
            {
                return NotFound();
            }

            //if (ModelState.IsValid)
            //{
            //    try
            //    {
            //        _context.Update(pullList);
            //        await _context.SaveChangesAsync();
            //    }
            //    catch (DbUpdateConcurrencyException)
            //    {
            //        if (!_context.PullLists.Any(p => p.Id == id))
            //        {
            //            return NotFound();
            //        }
            //        else
            //        {
            //            throw;
            //        }
            //    }
            //    return RedirectToAction("Details", id);
            //}
            //return View(pullList);
            return RedirectToAction("Details", "PullList", new { id = pullListid });
        }
    }
}
