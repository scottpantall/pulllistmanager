﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PullListManager.Models;
using System.Diagnostics;

namespace PullListManager.Controllers
{
    [AllowAnonymous]
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            return View();
        }

        public IActionResult About()
        {
            ViewData["Message"] = "PullListManager is your easy-to-use solution for managing your customer's pull list.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "All feedback, comments and requests welcomed!";

            return View();
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }

        public IActionResult NotAllowed()
        {
            return View();
        }
    }
}
