﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using PullListManager.Data;
using PullListManager.Models;
using System.Threading.Tasks;

namespace PullListManager.Controllers
{
    public class PullListRequestController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public PullListRequestController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;
        }

        [ActionName("_CreatePullListRequest")]
        public async Task<IActionResult> CreateAsync(int pullListId)
        {
            var currentUser = await _userManager.GetUserAsync(User);
            var currentPullList = await _context.PullLists.FindAsync(pullListId);
            if (currentUser.RoleName == "Owner" && currentUser.StoreId != currentPullList.StoreId)
            {
                // Show "Not Authorized" page
                return RedirectToAction("NotAllowed", "Home");
            }
            return View();
        }

        [ActionName("_CreatePullListRequest")]
        [HttpPost]
        public async Task<IActionResult> Create(int id, [Bind("RequestTitle")] PullListRequest pullListRequest)
        {
            var currentUser = await _userManager.GetUserAsync(User);
            var currentPullList = await _context.PullLists.FindAsync(id);
            if (currentUser.RoleName == "Owner" && currentUser.StoreId != currentPullList.StoreId)
            {
                // Show "Not Authorized" page
                return RedirectToAction("NotAllowed", "Home");
                
            }

            pullListRequest.PullListId = id;

            _context.Add(pullListRequest);
            await _context.SaveChangesAsync();
            return RedirectToAction("Details", "PullList", new { id = pullListRequest.PullListId });
        }

        public async Task<IActionResult> Delete(int id)
        {
            var pullListRequest = await _context.PullListRequests.FindAsync(id);
            _context.PullListRequests.Remove(pullListRequest);
            await _context.SaveChangesAsync();
            return RedirectToAction("Details", "PullList", new { id = pullListRequest.PullListId });
        }
    }
}
