﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PullListManager.Data;
using PullListManager.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PullListManager.Controllers
{
    public class StoreController : Controller
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;

        public StoreController(ApplicationDbContext context, UserManager<ApplicationUser> userManager)
        {
            _context = context;
            _userManager = userManager;

        }

        // GET: Store
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Index()
        {
            return View(await _context.Stores.ToListAsync());
        }

        // GET: Store/Details/5
        public async Task<IActionResult> Details(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var currentUser = await _userManager.GetUserAsync(User);
            if (currentUser.RoleName == "Owner" && currentUser.StoreId != id)
            {
                // Show "Not Authorized" page
                return RedirectToAction("NotAllowed", "Home");
            }

            var store = await _context.Stores
                .SingleOrDefaultAsync(m => m.Id == id);
            if (store == null)
            {
                return NotFound();
            }

            return View(store);
        }

        // GET: Store/Create
        [Authorize(Roles = "Admin")]
        public IActionResult Create()
        {
            return View();
        }

        // POST: Store/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Create([Bind("Id,Name,Address,City,State,ZipCode,Website,Phone")] Store store)
        {
            if (ModelState.IsValid)
            {
                _context.Add(store);
                await _context.SaveChangesAsync();
                return RedirectToAction(nameof(Index));
            }
            return View(store);
        }

        // GET: Store/Edit/5
        [Authorize(Roles = "Admin, Owner")]
        public async Task<IActionResult> Edit(int? id)
        {
            var currentUser = await _userManager.GetUserAsync(User);
            if (currentUser.RoleName == "Owner" && currentUser.StoreId != id)
            {
                // Show "Not Authorized" page
                return RedirectToAction("NotAllowed", "Home");
            }

            if (id == null)
            {
                return NotFound();
            }

            var store = await _context.Stores.SingleOrDefaultAsync(m => m.Id == id);
            if (store == null)
            {
                return NotFound();
            }
            return View(store);
        }

        // POST: Store/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<IActionResult> Edit(int id, [Bind("Id,Name,Address,City,State,ZipCode,Website,Phone")] Store store)
        {
            if (id != store.Id)
            {
                return NotFound();
            }

            var currentUser = await _userManager.GetUserAsync(User);
            if (currentUser.RoleName == "Owner" && currentUser.StoreId != id)
            {
                // Show "Not Authorized" page
                return RedirectToAction("NotAllowed", "Home");
            }

            if (ModelState.IsValid)
            {
                try
                {
                    _context.Update(store);
                    await _context.SaveChangesAsync();
                }
                catch (DbUpdateConcurrencyException)
                {
                    if (!StoreExists(store.Id))
                    {
                        return NotFound();
                    }
                    else
                    {
                        throw;
                    }
                }
                return RedirectToAction(nameof(Details), new { id = currentUser.StoreId });
            }
            return View(store);
        }

        // GET: Store/Delete/5
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            var store = await _context.Stores
                .SingleOrDefaultAsync(m => m.Id == id);
            if (store == null)
            {
                return NotFound();
            }

            return View(store);
        }

        // POST: Store/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public async Task<IActionResult> DeleteConfirmed(int id)
        {
            var store = await _context.Stores.SingleOrDefaultAsync(m => m.Id == id);
            _context.Stores.Remove(store);
            await _context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        // GET: Store/PrepOrder
        public async Task<IActionResult> PrepOrder(int id)
        {
            var currentUser = await _userManager.GetUserAsync(User);
            if (currentUser.RoleName == "Owner" && currentUser.StoreId != id)
            {
                // Show "Not Authorized" page
                return RedirectToAction("NotAllowed", "Home");
            }

            var pullLists = await _context.PullLists.Where(p => p.StoreId == id).OrderBy(p => p.CustomerName).ToListAsync();

            if (pullLists.Any())
            {
                return View(pullLists);
            }

            return RedirectToAction(nameof(Index));
        }

        // POST: Store/PrepOrder
        [HttpPost]
        public async Task<IActionResult> PrepOrder(string[] comicIds)
        {
            //var currentUser = await _userManager.GetUserAsync(User);
            //if (currentUser.RoleName == "Owner" && currentUser.StoreId != id)
            //{
            //    // Show "Not Authorized" page
            //    return RedirectToAction("NotAllowed", "Home");
            //}

            var books = new List<ComicBook>();

            foreach(var comicId in comicIds)
            {
                //Split the id string into Comic ID and Pull List Id
                string[] formValues = comicId.Split(':');

                //Create a ComicBook from Comic ID. 
                //Add PullListId to Comic Book.
                var pullListId = Int32.Parse(formValues[1]);
                var issue = await _context.ComicIssues.FindAsync(Guid.Parse(formValues[0]));
                var book = new ComicBook(issue, pullListId);

                //Add it to DB if it's not already there.
                books.Add(book);
            }

            //Check to see if any books have been unchecked. If so, remove them from the database
            //Take the list of all comic books marked as "SELECTED"
            var savedBooks = _context.ComicBooks.Where(c => c.BookStatus == BookStatus.SELECTED);
            //Foreach book in that list, see if they are in the books object by comparing Order Numbers and PullListIds
            foreach(var sBook in savedBooks)
            {
                //If the saved book is in the books object, remove it from the books object
                if (books.Any(b => b.OrderNumber == sBook.OrderNumber && b.PullListId == sBook.PullListId))
                {
                    var bookToRemove = books.SingleOrDefault(b => b.OrderNumber == sBook.OrderNumber && b.PullListId == sBook.PullListId);
                    books.Remove(bookToRemove);
                    
                }
                else //The saved book is NOT in the books object, remove it from the database
                {
                    _context.Remove(sBook);
                }
               
            }

            await _context.AddRangeAsync(books);
            await _context.SaveChangesAsync();
            return RedirectToAction("OrderPrepReport");
        }

        public IActionResult OrderPrepReport()
        {
            List<OrderPrepReportViewModel> orderPrepReportViewModels = new List<OrderPrepReportViewModel>();

            var uniqueIssues = _context.ComicBooks.Where(b => b.BookStatus == BookStatus.SELECTED).GroupBy(b => b.OrderNumber);
            foreach(var issue in uniqueIssues)
            {
                orderPrepReportViewModels.Add(new OrderPrepReportViewModel()
                {
                    ComicIssue = _context.ComicIssues.SingleOrDefault(c => c.OrderNumber == issue.Key),
                    Count = issue.Count(),
                });
            }

            return View(orderPrepReportViewModels);
        }

        private bool StoreExists(int id)
        {
            return _context.Stores.Any(e => e.Id == id);
        }
    }
}