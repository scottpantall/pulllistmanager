﻿using CsvHelper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using PullListManager.Data;
using PullListManager.Models;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace PullListManager.Controllers
{
    [Authorize(Roles = "Admin")]
    public class ComicIssueController : Controller
    {
        private readonly ApplicationDbContext _context;

        public ComicIssueController(ApplicationDbContext context)
        {
            _context = context;
        }

        public IActionResult UploadComics()
        {
            ViewData["UploadSuccess"] = "";
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> UploadComics(IFormFile file)
        {
            var books = ReadComicsFromPreviewOrder(file);

            if(!books.Any())
            {
                ViewData["UploadSuccess"] = "You did not upload a Previews Customer Order Form (Text Version)";
                return View();
            }

            if (_context.ComicIssues.Any())
            {
                if (_context.ComicIssues.FirstOrDefault().OrderNumber.Substring(0, 5) == books.FirstOrDefault().OrderNumber.Substring(0, 5))
                {
                    ViewData["UploadSuccess"] = "Comic info has already been submitted";
                    return View();
                }
            }
            

            //Remove all the old comic issues
            _context.RemoveRange(_context.ComicIssues?.ToList());
            await _context.SaveChangesAsync();

            //Add link to each record...
            foreach (var comic in books)
            {
                var orderSlug = string.Concat(comic.OrderNumber.Where(c => !char.IsWhiteSpace(c)));
                comic.PreviewsLink = "https://www.previewsworld.com/Catalog/" + orderSlug;
            }


            //Add the new comic issues
            _context.AddRange(books);
            await _context.SaveChangesAsync();


            ViewData["UploadSuccess"] = "Comic info uploaded from Diamond";
            return View();


        }

        public void SaveComicInfoFromPreviewsOrdeForm(IFormFile filename)
        {
            if (filename == null || filename.Length == 0)
                return;

            var path = Path.Combine(
                Directory.GetCurrentDirectory(), "wwwroot",
                "comics.txt");

            List<string> Allowed = new List<string>() { "FI", "GEM", "SS", "SPOT", "OA", "KIDS", "\t" };
            StreamReader file = new StreamReader(filename.OpenReadStream());
            StreamWriter writeFile = new StreamWriter(path);
            var line = file.ReadLine();
            while (line != "BOOKS")
            {
                line = file.ReadLine();
                foreach (string str in Allowed)
                {
                    if (line.StartsWith(str))
                    {
                        writeFile.WriteLine(line);
                    }
                }
            }

            file.Close();
            writeFile.Close();
        }

        public List<ComicIssue> ReadComicsFromPreviewOrder(IFormFile fileLocation)
        {
            // File doesn't exist...
            if (fileLocation == null || fileLocation.Length == 0)
                return new List<ComicIssue>();

            var path = Path.Combine(
                Directory.GetCurrentDirectory(), "wwwroot",
                "comics.txt");

            List<string> Allowed = new List<string>() { "FI", "GEM", "SS", "SPOT", "OA", "KIDS", "\t" };
            StreamReader file = new StreamReader(fileLocation.OpenReadStream());
            StreamWriter writeFile = new StreamWriter(path);
            var line = file.ReadLine();

            // File is not a Previews Order (Text Version)
            if (line.Substring(0, 8) != "PREVIEWS")
            {
                file.Close();
                writeFile.Close();
                return new List<ComicIssue>();
            }
                

            while (line != "ART BOOKS")
            {
                line = file.ReadLine();
                foreach (string str in Allowed)
                {
                    if (line.StartsWith(str))
                    {
                        writeFile.WriteLine(line);
                    }
                }
            }

            file.Close();
            writeFile.Close();

            StreamReader readFile = new StreamReader(path);
            var csv = new CsvReader(readFile);
            var map = new ComicIssueDTOMap();
            csv.Configuration.RegisterClassMap(map);
            csv.Configuration.Delimiter = "\t";
            csv.Configuration.DetectColumnCountChanges = false;
            csv.Configuration.HasHeaderRecord = false;
            csv.Configuration.MissingFieldFound = null;
            csv.Configuration.BadDataFound = null;

            var records = csv.GetRecords<ComicIssueDTO>();

            var books = new List<ComicIssue>();

            foreach (var r in records)
            {
                books.Add(new ComicIssue(r.OrderNumber, r.Title, r.DeliveryDate, r.SuggestedPrice));
            }

            readFile.Close();
            return books;
        }

    }
}