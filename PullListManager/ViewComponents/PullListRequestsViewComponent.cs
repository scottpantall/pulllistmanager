﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PullListManager.Data;
using PullListManager.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PullListManager.ViewComponents
{
    public class PullListRequestsViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext _dbContext;

        public PullListRequestsViewComponent(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IViewComponentResult> InvokeAsync(int pullListId)
        {
            var pullListRequests = await GetPullListRequestsAsync(pullListId);
            return View(pullListRequests);
        }

        private Task<List<PullListRequest>> GetPullListRequestsAsync(int pullListId)
        {
            return _dbContext.PullListRequests.Where(plr => plr.PullListId == pullListId).OrderBy(plr => plr.RequestTitle).ToListAsync();
        }
    }
}
