﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using PullListManager.Data;
using PullListManager.Models;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace PullListManager.ViewComponents
{
    [ViewComponent(Name = "PullListList")]
    public class PullListViewComponent : ViewComponent
    {
        private readonly ApplicationDbContext _dbContext;

        public PullListViewComponent(ApplicationDbContext dbContext)
        {
            _dbContext = dbContext;
        }

        public async Task<IViewComponentResult> InvokeAsync(int storeId)
        {
            var lists = await GetListsAsync(storeId);
            return View(lists);
        }

        private async Task<List<PullList>> GetListsAsync(int storeId)
        {
            var lists = await _dbContext.PullLists.Where(p => p.StoreId == storeId).ToListAsync();
            foreach(var list in lists)
            {
                list.Books = await _dbContext.ComicBooks.Where(b => b.PullListId == list.Id).ToListAsync();
            }
            return lists;
        }
    }
}
