﻿using System;
using System.Collections.Generic;

namespace PullListManager.Models
{
    public class PullList
    {
        public int Id { get; set; }
        public string CustomerName { get; set; }
        public string CustomerPhone { get; set; }
        public string CustomerEmail { get; set; }
        public decimal? DepositAmount { get; set; }
        public DateTime DateCreated { get; set; }
        public ICollection<PullListRequest> PullListRequests { get; set; }
        public bool IsActive { get; set; }
        public string Notes { get; set; }
        public ICollection<ComicBook> Books { get; set; }
        public int StoreId { get; set; }
        public DateTime? LastPickupDate { get; set; }
    }
}