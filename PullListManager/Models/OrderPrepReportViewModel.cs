﻿namespace PullListManager.Models
{
    public class OrderPrepReportViewModel
    {
        public ComicIssue ComicIssue { get; set; }
        public int Count { get; set; }
    }
}