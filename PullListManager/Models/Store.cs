﻿using System.Collections.Generic;

namespace PullListManager.Models
{
    public class Store
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string ZipCode { get; set; }
        public string Website { get; set; }
        public string Phone { get; set; }
        public string Slug { get; set; }
        public ICollection<ApplicationUser> Employees { get; set; }
        public ICollection<PullList> PullLists { get; set; }
    }
}