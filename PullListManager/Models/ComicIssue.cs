﻿using System;

namespace PullListManager.Models
{
    public class ComicIssue
    {
        public Guid Id { get; set; }
        public string OrderNumber { get; set; }
        public string Title { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public string SuggestedPrice { get; set; }
        public string PreviewsLink { get; set; }

        public ComicIssue(string orderNumber, string title, DateTime? deliveryDate, string suggestedPrice)
        {
            Id = Guid.NewGuid();
            OrderNumber = orderNumber;
            Title = title;
            DeliveryDate = deliveryDate;
            SuggestedPrice = suggestedPrice;
        }

        public ComicIssue()
        {

        }
    }
}
