﻿using Microsoft.AspNetCore.Identity;

namespace PullListManager.Models
{
    // Add profile data for application users by adding properties to the ApplicationUser class
    public class ApplicationUser : IdentityUser
    {
        public string Name { get; set; }
        public string RoleName { get; set; }
        public int StoreId { get; set; }
        public bool IsVerified { get; set; }
    }
}
