﻿namespace PullListManager.Models
{
    public enum BookStatus
    {
        SELECTED,
        ORDERED,
        DELIVERED,
        DELAYED,
        IN_STORE
    }
}