﻿using System;

namespace PullListManager.Models
{
    public class ComicBook
    {
        public Guid Id { get; set; }
        public string OrderNumber { get; set; }
        public string Title { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public BookStatus BookStatus { get; set; }
        public int PullListId { get; set; }
        public string PreviewsLink { get; set; }

        public ComicBook()
        {
        }

        public ComicBook(ComicIssue comicIssue, int pullListId)
        {
            Id = Guid.NewGuid();
            OrderNumber = comicIssue.OrderNumber;
            Title = comicIssue.Title;
            DeliveryDate = comicIssue.DeliveryDate;
            PreviewsLink = comicIssue.PreviewsLink;
            BookStatus = BookStatus.SELECTED;
            PullListId = pullListId;
        }
    }
}