﻿using CsvHelper.Configuration;

namespace PullListManager.Models
{
    public class ComicIssueDTOMap : ClassMap<ComicIssueDTO>
    {
        public ComicIssueDTOMap()
        {
            Map(m => m.Thing);
            Map(m => m.OrderNumber);
            Map(m => m.Title);
            Map(m => m.DeliveryDate);
            Map(m => m.SuggestedPrice);
        }
    }
}