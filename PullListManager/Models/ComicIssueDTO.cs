﻿using System;

namespace PullListManager.Models
{
    public class ComicIssueDTO
    {
        public string Thing { get; set; }
        public string OrderNumber { get; set; }
        public string Title { get; set; }
        public DateTime? DeliveryDate { get; set; }
        public string SuggestedPrice { get; set; }
    }
}
