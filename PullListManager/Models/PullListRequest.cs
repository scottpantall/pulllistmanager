﻿namespace PullListManager.Models
{
    public class PullListRequest
    {
        public int Id { get; set; }
        public string RequestTitle { get; set; }
        public int PullListId { get; set; }
    }
}